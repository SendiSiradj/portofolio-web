from django.shortcuts import render
from django.http import HttpResponse
from .models import Feedback
from .forms import FeedbackForm
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def index(request):
    print(request.POST)
    if (request.method == "POST"):
        name = request.POST.get("Name")
        email = request.POST.get("Email")
        message = request.POST.get("Message")
        # print("Nama : " + name)
        # print("Email : " + email)
        # print("Message : " + message)

        # HEROKU CAN'T USE SQLITE3 BAKA
        record = Feedback(name=name, email=email, message=message)
        record.save()
        return render(request, 'main.html')
    else :
        return render(request, 'main.html')

from django import forms
from .models import Feedback
from django.forms import ModelForm

class FeedbackForm(forms.ModelForm):
    # name = forms.CharField(label='Nama ', max_length=100)
    # email = forms.EmailField(label="Email", max_length = 50)
    # message = forms.CharField(label="Message", max_length=200)

    class Meta:
        model = Feedback
        fields = ["name","email","message"]
